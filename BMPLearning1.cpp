﻿#include "pch.h"
#include <iostream>
#include <fstream>
#include <bitset>
#include <string>
#include <stdint.h>
#include <stdio.h>
#include <vector>
#include <windows.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")

using namespace std;
LRESULT CALLBACK WndProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);

struct WindowsShit
{
	HINSTANCE hInstance;
	HINSTANCE hPrevInstance;
	LPSTR lpCmdLine;
	int nCmdShow;
	uint32_t width;
	uint32_t height;
};

WindowsShit govno;

struct RGBColor
{
	uint8_t Red = 0;
	uint8_t Green = 0;
	uint8_t Blue = 0;
};

struct Line
{
	uint32_t x1;
	uint32_t y1;
	uint32_t x2;
	uint32_t y2;
	RGBColor color;
};

struct Circle
{
	int xCenter;
	int yCenter;
	int Radius;
	bool fill = false;
};

struct Square
{
	int xBottom;
	int yBottom;
	int xTop;
	int yTop;
	RGBColor color;
	bool fill = false;
};

VOID OnPaint(HDC* hdc)
{
	Gdiplus::Graphics graphics(*hdc);
	Gdiplus::Image    image(L"D:\\_FirstLine!.bmp");
	graphics.SetInterpolationMode(Gdiplus::InterpolationModeNearestNeighbor);
	graphics.DrawImage(&image, 0, 0, govno.width, govno.height);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	RECT Rect;
	HDC hdc;

	switch (messg)
	{
	case WM_PAINT:
		GetClientRect(hWnd, &Rect);
		hdc = BeginPaint(hWnd, &ps);
		OnPaint(&hdc);
		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return (DefWindowProc(hWnd, messg, wParam, lParam));
	}
	return (0);
}

bool _CreateWindow(uint32_t width, uint32_t height)
{
	HWND hWnd;
	MSG lpMsg;
	WNDCLASS wc;

	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;

	// Initialize GDI+.
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = govno.hInstance;
	wc.hIcon = NULL;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = (LPCWSTR)"CG_WAPI_Template";

	// Регистрируем класс окна
	if (!RegisterClass(&wc))
	{
		MessageBox(NULL, (LPCWSTR)"Can't register a window class!", (LPCWSTR)"ERROR", MB_OK);
		return 0;
	}

	// Создаем основное окно приложения
	hWnd = CreateWindow(
		wc.lpszClassName, // class name
		L"My Bitmap", // head name
		WS_OVERLAPPEDWINDOW, // wndow style
		50, 50, // window position
		width, height, // width and height
		(HWND)NULL, // parent window
		(HMENU)NULL,
		(HINSTANCE)wc.hInstance, // pointer to current application instance
		NULL); // Передается в качестве lParam в событие WM_CREATE

	if (!hWnd)
	{
		MessageBox(NULL, (LPCWSTR)"Unable to create a window!", (LPCWSTR)"ERROR", MB_OK);
		return 0;
	}

	// Показываем наше окно
	ShowWindow(hWnd, govno.nCmdShow);
	UpdateWindow(hWnd);

	// Выполняем цикл обработки сообщений до закрытия приложения
	while (GetMessage(&lpMsg, NULL, 0, 0))
	{
		TranslateMessage(&lpMsg);
		DispatchMessage(&lpMsg);
	}
	Gdiplus::GdiplusShutdown(gdiplusToken);
	return (lpMsg.wParam);
}

class BMPWrapper
{
	struct BMPHeader
	{
		uint16_t signature	=0;	//signature, must be 4D42 hex
		uint32_t size		=0;	//size of BMP file in bytes (unreliable)
		uint16_t reserved1	=0;
		uint16_t reserved2	=0;
		uint32_t offset		=0;	//offset to start of image data in bytes
		uint32_t headerSize	=0;	//size of BITMAPINFOHEADER structure, must be 40
		uint32_t width		=0;	//image width in pixels
		uint32_t height		=0;	//image height in pixels
		uint16_t planes		=0;	//number of planes in the image, must be 1
		uint16_t BPP		=0;	//number of bits per pixel(1, 4, 8, or 24)
		uint32_t compressionType		=0;	//compression type(0 = none, 1 = RLE - 8, 2 = RLE - 4)
		uint32_t imageSize				=0;	//size of image data in bytes (including padding)
		uint32_t horizontalResolution	=0;	//horizontal resolution in pixels per meter (unreliable)
		uint32_t verticalResolution		=0;	//vertical resolution in pixels per meter (unreliable)
		uint32_t numberOfColors			=0;	//number of colors in image, or zero
		uint32_t numberImportantColors	=0; //number of important colors, or zero
	};
public:
	BMPWrapper()	{	 }
	BMPWrapper(char* filename) { if (!mIsLoaded) { LoadFromFile(filename); } }
	~BMPWrapper()	{	 }
	BMPWrapper(BMPWrapper& Obj);

	bool LoadFromFile(const char* filename);
	bool SaveFileTo(const char* filename);
	bool CreateNewFile(uint32_t Width, uint32_t Height);
	bool SetPixel(uint32_t x, uint32_t y, RGBColor color);

	void PrintPlane();
	void PrintHeader();

	uint16_t GetSignature()				{ return mHeader.signature;			}
	uint32_t GetSize()					{ return mHeader.size;				}
	uint16_t GetReserved1()				{ return mHeader.reserved1;			}
	uint16_t GetReserved2()				{ return mHeader.reserved2;			}
	uint32_t GetOffset()				{ return mHeader.offset;			}
	uint32_t GetHeaderSize()			{ return mHeader.headerSize;		}
	uint32_t GetWidth()					{ return mHeader.width;				}
	uint32_t GetHeight()				{ return mHeader.height;			}
	uint16_t GetPlanes()				{ return mHeader.planes;			}
	uint16_t GetBPP()					{ return mHeader.BPP;				}
	uint32_t GetCompressionType()		{ return mHeader.compressionType;	}
	uint32_t GetImageSize()				{ return mHeader.imageSize;			}
	uint32_t GetHorizontalResolution()	{ return mHeader.horizontalResolution;	}
	uint32_t GetVerticalResolution()	{ return mHeader.verticalResolution;	}
	uint32_t GetNumberOfColors()		{ return mHeader.numberOfColors;		}
	uint32_t GetImportantColors()		{ return mHeader.numberImportantColors; }
	
private:
	bool			mIsLoaded = false;
	BMPHeader		mHeader;
	FILE*			mLoadedFile	= nullptr;
	FILE*			mSaveFile	= nullptr;
	uint32_t		mPlaneWidth		= 0;
	uint32_t		mPlaneHeight	= 0;
	uint8_t			mPadding	= 0;
	uint32_t		mPlaneSize	= 0;
	vector<RGBColor> mPlane;
	
	void mReadFileHeader(const char* filename);
	void mInitPlane		(const char* filename);
	char mGuessColor(RGBColor pixel);
};

BMPWrapper::BMPWrapper(BMPWrapper& Obj)
{
	mHeader = Obj.mHeader;
	mIsLoaded = Obj.mIsLoaded;
	mLoadedFile = Obj.mLoadedFile;
	mSaveFile = Obj.mSaveFile;
	mPlaneWidth = Obj.mPlaneWidth;
	mPlaneHeight = Obj.mPlaneHeight;
	mPadding = Obj.mPadding;
	mPlaneSize = Obj.mPlaneSize;
	mPlane = Obj.mPlane;
}

bool BMPWrapper::LoadFromFile(const char* filename)
{
	if (mIsLoaded)
	{
		return false;
	}
	fopen_s(&mLoadedFile, filename, "rb"); 
	mReadFileHeader(filename);
	mInitPlane(filename);
	mIsLoaded = true;
	fclose(mLoadedFile);
	mLoadedFile = nullptr;
	return true;
}

bool BMPWrapper::SaveFileTo(const char* filename)
{
	if (!mIsLoaded)
	{
		return false;
	}
	fopen_s(&mSaveFile, filename, "wb");

	fwrite(&mHeader.signature, sizeof(mHeader.signature), 1, mSaveFile);
	fwrite(&mHeader.size, sizeof(mHeader.size), 1, mSaveFile);
	fwrite(&mHeader.reserved1, sizeof(mHeader.reserved1), 1, mSaveFile);
	fwrite(&mHeader.reserved2, sizeof(mHeader.reserved2), 1, mSaveFile);
	fwrite(&mHeader.offset, sizeof(mHeader.offset), 1, mSaveFile);
	fwrite(&mHeader.headerSize, sizeof(mHeader.headerSize), 1, mSaveFile);
	fwrite(&mHeader.width, sizeof(mHeader.width), 1, mSaveFile);
	fwrite(&mHeader.height, sizeof(mHeader.height), 1, mSaveFile);
	fwrite(&mHeader.planes, sizeof(mHeader.planes), 1, mSaveFile);
	fwrite(&mHeader.BPP, sizeof(mHeader.BPP), 1, mSaveFile);
	fwrite(&mHeader.compressionType, sizeof(mHeader.compressionType), 1, mSaveFile);
	fwrite(&mHeader.imageSize, sizeof(mHeader.imageSize), 1, mSaveFile);
	fwrite(&mHeader.horizontalResolution, sizeof(mHeader.horizontalResolution), 1, mSaveFile);
	fwrite(&mHeader.verticalResolution, sizeof(mHeader.verticalResolution), 1, mSaveFile);
	fwrite(&mHeader.numberOfColors, sizeof(mHeader.numberOfColors), 1, mSaveFile);
	fwrite(&mHeader.numberImportantColors, sizeof(mHeader.numberImportantColors), 1, mSaveFile);

	for (uint32_t i = 0; i < mPlaneSize; i++)
	{
		fwrite(&mPlane[i].Blue, sizeof(uint8_t), 1, mSaveFile);
		fwrite(&mPlane[i].Green, sizeof(uint8_t), 1, mSaveFile);
		fwrite(&mPlane[i].Red, sizeof(uint8_t), 1, mSaveFile);
		if ((i + 1) % (mPlaneWidth) == 0 && (i + 1) >= (mPlaneWidth))
		{
			fwrite(mSaveFile, sizeof(uint8_t), mPadding, mSaveFile);
		}
	}
	fclose(mSaveFile);
	mSaveFile = nullptr;
	return true;
}

bool BMPWrapper::CreateNewFile(uint32_t Width, uint32_t Height)
{
	if (mIsLoaded)
	{
		return false;
	}
	mHeader.signature = uint16_t(strtoul("4d42", 0, 16));
	mHeader.size = 0; //???
	mHeader.reserved1 = 0;
	mHeader.reserved2 = 0;
	mHeader.offset = 54;
	mHeader.headerSize = 40;
	mHeader.width = Width;
	mHeader.height = Height;
	mHeader.planes = 1;
	mHeader.BPP = 24;
	mHeader.compressionType = 0;
	mHeader.imageSize = 0;
	mHeader.horizontalResolution = 0;
	mHeader.verticalResolution = 0;
	mHeader.numberOfColors = 0;
	mHeader.numberImportantColors = 0;

	mPlane.resize(Width * Height, { 192,192,192 });
	mPlaneWidth = Width;
	mPlaneHeight = Height;
	mPadding = (mPlaneWidth * 3) % 4;
	mPlaneSize = mPlaneWidth * mPlaneHeight;
	mIsLoaded = true;

	return true;
}

bool BMPWrapper::SetPixel(uint32_t x, uint32_t y, RGBColor color)
{
	if (!mIsLoaded)
	{
		return false;
	}
	if (x > mPlaneWidth || y > mPlaneHeight || x < 0 || y < 0)
	{
		return false;
	}
	mPlane[y*mPlaneWidth + x] = color;
	return true;
}


void BMPWrapper::mReadFileHeader(const char* filename)
{
	fread(&mHeader.signature, sizeof(mHeader.signature), 1,mLoadedFile);
	fread(&mHeader.size, sizeof(mHeader.size), 1, mLoadedFile);
	fread(&mHeader.reserved1, sizeof(mHeader.reserved1), 1, mLoadedFile);
	fread(&mHeader.reserved2, sizeof(mHeader.reserved2), 1, mLoadedFile);
	fread(&mHeader.offset, sizeof(mHeader.offset), 1, mLoadedFile);
	fread(&mHeader.headerSize, sizeof(mHeader.headerSize), 1, mLoadedFile);
	fread(&mHeader.width, sizeof(mHeader.width), 1, mLoadedFile);
	fread(&mHeader.height, sizeof(mHeader.height), 1, mLoadedFile);
	fread(&mHeader.planes, sizeof(mHeader.planes), 1, mLoadedFile);
	fread(&mHeader.BPP, sizeof(mHeader.BPP), 1, mLoadedFile);
	fread(&mHeader.compressionType, sizeof(mHeader.compressionType), 1, mLoadedFile);
	fread(&mHeader.imageSize, sizeof(mHeader.imageSize), 1, mLoadedFile);
	fread(&mHeader.horizontalResolution, sizeof(mHeader.horizontalResolution), 1, mLoadedFile);
	fread(&mHeader.verticalResolution, sizeof(mHeader.verticalResolution), 1, mLoadedFile);
	fread(&mHeader.numberOfColors, sizeof(mHeader.numberOfColors), 1, mLoadedFile);
	fread(&mHeader.numberImportantColors, sizeof(mHeader.numberImportantColors), 1, mLoadedFile);
}

void BMPWrapper::PrintHeader(void)
{
	cout << std::hex << "Signature: " << GetSignature() << endl;                        //signature, must be 4D42 hex
	cout << std::dec << "Size: " << float(GetSize()) / (1024 * 1024) << " MB" << endl;  //size of BMP file in bytes (unreliable)
	cout << std::dec << "Reserved first: " << GetReserved1() << endl;
	cout << std::dec << "Reserved second: " << GetReserved2() << endl;
	cout << std::dec << "Offset: " << GetOffset() << endl;                              //offset to start of image data in bytes
	cout << std::dec << "Header size: " << GetHeaderSize() << endl;                     //size of BITMAPINFOHEADER structure, must be 40
	cout << std::dec << "Width: " << GetWidth() << endl;                                //image width in pixels
	cout << std::dec << "Height: " << GetHeight() << endl;                              //image height in pixels
	cout << std::dec << "Planes: " << GetPlanes() << endl;                              //number of planes in the image, must be 1
	cout << std::dec << "Bits Per Pixel: " << GetBPP() << endl;                         //number of bits per pixel(1, 4, 8, or 24)
	cout << std::dec << "Compresson type: " << GetCompressionType() << endl;            //compression type(0 = none, 1 = RLE - 8, 2 = RLE - 4)
	cout << std::dec << "Image size: " << GetImageSize() << " bytes" << endl;           //size of image data in bytes (including padding)
	cout << std::dec << "Horizontal resolution: " << GetHorizontalResolution() << endl; //horizontal resolution in pixels per meter (unreliable)
	cout << std::dec << "Vertical resolution: " << GetVerticalResolution() << endl;     //vertical resolution in pixels per meter (unreliable)
	cout << std::dec << "Number of colors: " << GetNumberOfColors() << endl;            //number of colors in image, or zero
	cout << std::dec << "Number of important colors: " << GetImportantColors() << endl; //number of important colors, or zero
}

void BMPWrapper::mInitPlane(const char* filename)
{
	mPlaneWidth		= GetWidth();
	mPlaneHeight	= GetHeight();
	mPadding		= (mPlaneWidth * 3) % 4;
	
	mPlaneSize = mPlaneWidth * mPlaneHeight;

	mPlane.resize(mPlaneSize);

	fseek(mLoadedFile, 54, SEEK_SET);

	for (uint32_t i = 0; i < mPlaneSize; i++)
	{
		fread(&mPlane[i].Blue,	sizeof(uint8_t), 1, mLoadedFile);
		fread(&mPlane[i].Green,	sizeof(uint8_t), 1, mLoadedFile);
		fread(&mPlane[i].Red,	sizeof(uint8_t), 1, mLoadedFile);

		if ((i+1) % (mPlaneWidth) == 0 && (i+1) >= (mPlaneWidth))
		{
			fseek(mLoadedFile, mPadding, SEEK_CUR);
		}
	}
}

char BMPWrapper::mGuessColor(RGBColor pixel)
{
	if (pixel.Blue > 1 && pixel.Green == 0 && pixel.Red == 0)
	{
		return 'B'; //Blue
	}
	else if (pixel.Blue == 0 && pixel.Green > 0 && pixel.Red == 0)
	{
		return 'G'; //Green
	}
	else if (pixel.Blue == 0 && pixel.Green == 0 && pixel.Red > 0)
	{
		return 'R'; //Red
	}
	else if (pixel.Blue == 0 && pixel.Green > 0 && pixel.Red > 0)
	{
		return 'Y'; //Yellow
	}
	else if (pixel.Blue > 0 && pixel.Green == 0 && pixel.Red > 0)
	{
		return 'M'; //Magenta
	}
	else if (pixel.Blue == 0 && pixel.Green == 0 && pixel.Red == 0)
	{
		return 'X'; //Black
	}
	else if (pixel.Blue >= 250 && pixel.Green >= 250 && pixel.Red >= 250)
	{
		return 'W'; //White
	}
	else
	{
		return 'U'; //Unknown
	}
	return '$';
}

void BMPWrapper::PrintPlane()
{
	cout << "The plane: " << endl;
	for (int32_t i = mPlaneSize - mPlaneWidth; i >= 0; i = i - mPlaneWidth)
	{
		for (uint32_t j = 0; j < mPlaneWidth; j++)
		{
			cout << '[' << mGuessColor(mPlane[i+j]) << ']';
		}
		cout << endl;
	}
}


class BMPDraw
{
public:
	BMPDraw() { mBMP = nullptr; }
	BMPDraw(BMPWrapper* Obj) { mBMP = Obj; }
	BMPDraw(BMPDraw& Obj) { mBMP = Obj.mBMP; }
	~BMPDraw() {  }

	bool DrawPixel(uint32_t x, uint32_t y, RGBColor color);
	bool DrawLine(Line line);
	bool DrawLineBrianWill(Line line);
	bool DrawShaLine1(Line line);
	bool DrawShaLine2(Line line);
	bool DrawShaLine3(Line line);
	bool DrawCirle(Circle circle);

private:
	BMPWrapper* mBMP;
	bool DrawStraightLine(Line line);
};



bool BMPDraw::DrawPixel(uint32_t x, uint32_t y, RGBColor color)
{
	return mBMP->SetPixel(x, y, color);
}

bool BMPDraw::DrawShaLine1(Line line)
{
	//right to left case
	if (line.x2 < line.x1)
	{
		swap(line.x1, line.x2);
		swap(line.y1, line.y2);
	}

	int32_t slope = 1;
	//slope
	if (line.y1 > line.y2)
	{
		slope = -1;
	}

	float m = float(int32_t(line.y2) - int32_t(line.y1))/float(int32_t(line.x2) - int32_t(line.x1));

	//line angle more than 45
	uint32_t x = line.x1;
	uint32_t y = line.y1;
	if (fabs(m) >= 1)
	{
		for (y; y <= line.y2; y = y + slope)
		{
			DrawPixel(x, y, line.color);
			float mnew = float(int32_t(line.y2) - int32_t(y)) / float(int32_t(line.x2) - int32_t(x));
			if (fabs(m - mnew) > 0.5)
			{
				x++;
			}
		}
	}
	else if(fabs(m) < 1)
	{
		for (x; x <= line.x2; x++)
		{
			DrawPixel(x, y, line.color);
			float mnew = float(int32_t(line.y2) - int32_t(y)) / float(int32_t(line.x2) - int32_t(x));
			if (fabs(m - mnew) > 0.5)
			{
				y = y + slope;
			}
		}
	}
	return true;
}

bool BMPDraw::DrawShaLine2(Line line)
{
	float x1 = line.x1;
	float x2 = line.x2;
	float y1 = line.y1;
	float y2 = line.y2;

	//right to left case
	if (x2 < x1)
	{
		swap(x1, x2);
		swap(y1, y2);
	}

	int32_t slope = 1;
	//slope
	if (y1 > y2)
	{
		slope = -1;
	}

	float m = (y2 - y1) / (x2 - x1);

	//line angle more than 45
	float x = x1;
	float y = y1;

	for (x; x <= line.x2; x = x + 1)
	{
		float newm = (y2 - y) / (x2 - x);
		if (fabs(m - newm) > 0.5)
		{
			y = y + slope;
		}
		DrawPixel(x, y, line.color);
	}
	return true;

}

bool BMPDraw::DrawShaLine3(Line line)
{
	uint32_t x1 = line.x1;
	uint32_t x2 = line.x2;
	uint32_t y1 = line.y1;
	uint32_t y2 = line.y2;
	
	if (x1 == x2 == y1 == y2)
	{
		DrawPixel(x1, y1, line.color);
		return true;
	}

	if (x1 == x2 || y1 == y2)
	{
		return DrawStraightLine(line);
	}

	//right to left case
	if (x2 < x1)
	{
		swap(x1, x2);
		swap(y1, y2);
	}

	int8_t slope = 1;
	//slope
	if (y1 > y2)
	{
		slope = -1;
	}

	float m = (int32_t(y2) - y1) / (int32_t(x2) - x1);

	int32_t deltax = x2 - x1;
	int32_t deltay;
	if (slope > 0)
	{
		deltay = y2 - y1;
	}
	else if (slope < 0)
	{
		deltay = y1 - y2;
	}

	float x = x1;
	float y = y1;

	if (fabs(m) >= 1)
	{
		float step = deltax;
		float stepsize = float(deltay) / deltax; //длинна колбаски
		for (x; x < x2; x = x + 1)
		{
			if (slope == -1)
			{
				uint32_t newY = y + (stepsize * slope);
				if (newY < y2)
				{
					newY = y2;
				}
				Line tmpLine = { x, y, x, newY, line.color };
				DrawStraightLine(tmpLine);
				y = (y + (stepsize * slope));
			}
			else if (slope == 1)
			{
				uint32_t newY = y + stepsize;
				if (newY > y2)
				{
					newY = y2;
				}
				Line tmpLine = { x, y, x, newY, line.color };
				DrawStraightLine(tmpLine);
				y = y + stepsize;
			}
		}
	}
	else if (fabs(m) < 1)
	{
		float step = deltay;
		float stepsize = float(deltax) / deltay; //длинна колбаски
		for (y; y != y2; y = y + slope)
		{
			uint32_t newX = x + stepsize;
			if (newX > x2)
			{
				newX = x2;
			}
			Line tmpLine = { x, y, newX, y, line.color };
			DrawStraightLine(tmpLine);
			x = x + stepsize;
		}
	}
	else
	{
		system("xuy");
		exit(0);
	}
	return true;
}


bool BMPDraw::DrawLineBrianWill(Line line)
{
	int32_t rise = int32_t(line.y2) - int32_t(line.y1);
	int32_t run = int32_t(line.x2) - int32_t(line.x1);

	float m = float(rise) / (float)run; //коэффициент наклона, угловой коэффициент
	// Если m больше 1 то линия имее тнаклон больше 45 градусов, при m = 99999 линия будет практически вертикальной
	// Если m между -1 и 1, то линия будет горизонтальной, чем меньше abs(m) тем больше линия стремиться к ----

	int8_t step; //шаг увеличения y
	if (m >= 0)
	{
		step = 1; //линия идет вверх
	}
	else
	{
		step = -1; //линия идет вниз
	}

	float offset = 0; // с каждым шагом к оффсету прибавляется угловой коэффициент m
	float threshold = 0.5; //некий индикатор относительно которого мы определяем увеличивать у или не надо
	if (fabs(m) >= 1) // Если линия круто идет вверх
	{
		uint32_t y = line.y1;
		
		if (line.x2 < line.x1)
		{
			swap(line.x1, line.x2);
			y = line.y2;
		}

		for (uint32_t i = line.x1; i < line.x2; i++)
		{
			DrawPixel(i, y, line.color);
			offset = offset + fabs(m);
			if (offset >= threshold)
			{
				y = y + step;
				threshold = threshold + 1;
			}
		}
	}
	else if (fabs(m) < 1) // если линия стремиться быть горизонтальной
	{
		m = float(run) / float(rise);
		uint32_t x = line.x1;
		
		if (line.y2 < line.y1)
		{
			swap(line.y1, line.y2);
			x = line.x2;
		}

		for (uint32_t i = line.y1; i < line.y2; i++)
		{
			DrawPixel(x, i, line.color);
			offset = offset + m;
			if (offset >= threshold)
			{
				x = x + step;
				threshold = threshold + 1;
			}
		}
	}
	return true;
}

bool BMPDraw::DrawStraightLine(Line line)
{
	int32_t sizeY = abs(int32_t(line.y1) - int32_t(line.y2));
	int32_t sizeX = abs(int32_t(line.x1) - int32_t(line.x2));
	if (line.x1 == line.x2)
	{
		for (int32_t i = 0; i <= sizeY; i++)
		{
			if (line.y1 < line.y2)
			{
				DrawPixel(line.x1, line.y1 + i, line.color);
			}
			else
			{
				DrawPixel(line.x1, line.y1 - i, line.color);
			}
		}
	}
	else if (line.y1 == line.y2)
	{
		for (int i = 0; i <= sizeX; i++)
		{
			if (line.x1 < line.x2)
			{
				DrawPixel(line.x1 + i, line.y1, line.color);
			}
			else
			{
				DrawPixel(line.x1 - i, line.y1, line.color);
			}
		}
	}
	return true;
}

bool BMPDraw::DrawLine(Line line)
{
	line.color.Blue = 255;
	line.color.Red = 255;
	line.color.Green = 0;
	if (line.x1 == line.x2 == line.y1 == line.y2)
	{
		DrawPixel(line.x1, line.y1, line.color);
		return true;
	}

	if (line.x1 == line.x2 || line.y1 == line.y2)
	{
		return DrawStraightLine(line);
	}

	bool steep = (fabs(line.y2 - line.y1) > fabs(line.x2 - line.x1));
	if(steep)
	{
		swap(line.x1, line.y1);
		swap(line.x2, line.y2);
	}

	if(line.x1 > line.x2)
	{
		swap(line.x1, line.x2);
		swap(line.y1, line.y2);
	}

	uint32_t dx = line.x2 - line.x1;
	uint32_t dy = abs(int32_t(line.y2) - int32_t(line.y1));

	float error = float(dx) / 2.0f;
	int ystep;
		if (line.y1 < line.y2)
		{
			ystep = 1;
		}
		else
		{
			ystep = -1;
		}
	int y = (int)line.y1;

	const int maxX = (int)line.x2;

	for (int x = (int)line.x1; x < maxX; x++)
	{
		if (steep)
		{
			DrawPixel(y, x, line.color);
		}
		else
		{
			DrawPixel(x, y, line.color);
		}

		error = error - dy;
		if (error < 0)
		{
			y += ystep;
			error += dx;
		}
	}

	return true;;
}

bool BMPDraw::DrawCirle(Circle circle)
{
	return false;
}

void UseCase1()
{
	char filepath[] = "D:\\CPP_BMP.bmp";
	BMPWrapper myBMP(filepath);
	myBMP.PrintHeader();
	myBMP.PrintPlane();
	RGBColor black = { 0,0,0 };
	myBMP.SetPixel(0, 2, black);
	myBMP.SetPixel(3, 2, black);
	myBMP.SetPixel(5, 6, black);
	myBMP.SetPixel(9, 0, black);
	myBMP.SaveFileTo("D:\\AfterChanges.bmp");
	//system("pause");
}

void UseCase2()
{
	BMPWrapper myBMP;
	myBMP.CreateNewFile(234, 567);
	myBMP.PrintHeader();
	RGBColor black = { 0,0,0 };
	myBMP.SetPixel(0, 2, black);
	myBMP.SetPixel(3, 2, black);
	myBMP.SetPixel(5, 6, black);
	myBMP.SetPixel(9, 0, black);
	myBMP.SaveFileTo("D:\\BrandNew.bmp");
	//system("pause");
}

void UseCase3() //using Draw class
{
	BMPWrapper myBMP;
	myBMP.CreateNewFile(govno.width, govno.height);
	myBMP.PrintHeader();
	RGBColor black = { 0,0,0 };
	RGBColor red = { 255,0,0 };
	RGBColor green = { 0,255,0 };
	RGBColor blue = { 0,0,255 };
	RGBColor white = { 255,255,255 };

	BMPDraw Artist(&myBMP);
	Line newLine1 = { 50,500,40,100,black };
	Line newLine2 = { 50,50,90,100,red };
	Line newLine3 = { 50,500,100,200,green };
	Line newLine4 = { 50,50,150,450,blue };

	Line newLine5 = { 50,500,600,400,black };
	Line newLine6 = { 50,50,500,100,red };
	Line newLine7 = { 50,500,900,200,green };
	Line newLine8 = { 50,50,950,450,blue };
	
	Line straight1 =	{ 0, 50, govno.width, 50, white };
	Line straight2 =	{ 0, 100, govno.width, 100, white };
	Line straight3 =	{ 0, 150, govno.width, 150, white };
	Line straight4 =	{ 0, 200, govno.width, 200, white };
	Line straight5 =	{ 0, 250, govno.width, 250, white };
	Line straight6 =	{ 0, 300, govno.width, 300, white };
	Line straight7 =	{ 0, 350, govno.width, 350, white };
	Line straight8 =	{ 0, 400, govno.width, 400, white };
	Line straight9 =	{ 0, 450, govno.width, 450, white };
	Line straight10 =	{ 0, 500, govno.width, 500, white };

	Artist.DrawLine(straight1);
	Artist.DrawLine(straight2);
	Artist.DrawLine(straight3);
	Artist.DrawLine(straight4);
	Artist.DrawLine(straight5);
	Artist.DrawLine(straight6);
	Artist.DrawLine(straight7);
	Artist.DrawLine(straight8);
	Artist.DrawLine(straight9);
	Artist.DrawLine(straight10);


	Artist.DrawShaLine3(newLine1);
	Artist.DrawShaLine3(newLine2);
	Artist.DrawShaLine3(newLine3);
	Artist.DrawShaLine3(newLine4);
	Artist.DrawShaLine3(newLine5);
	Artist.DrawShaLine3(newLine6);
	Artist.DrawShaLine3(newLine7);
	Artist.DrawShaLine3(newLine8);

	Artist.DrawLine(newLine1);
	Artist.DrawLine(newLine2);
	Artist.DrawLine(newLine3);
	Artist.DrawLine(newLine4);
	Artist.DrawLine(newLine5);
	Artist.DrawLine(newLine6);
	Artist.DrawLine(newLine7);
	Artist.DrawLine(newLine8);

	//Artist.DrawLine(newLine1);
	//Artist.DrawLine(newLine2);
	//Artist.DrawLine(newLine3);
	//Artist.DrawLine(newLine4);
	myBMP.SaveFileTo("D:\\_FirstLine!.bmp");
	_CreateWindow(govno.width, govno.height);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	govno.hInstance = hInstance;
	govno.hPrevInstance = hPrevInstance;
	govno.lpCmdLine = lpCmdLine;
	govno.nCmdShow = nCmdShow;
	govno.width = 1000;
	govno.height = 1000;
	//UseCase1();
	//UseCase2();
	UseCase3();
	cout << "Helloween Cruel Word!\n";
	return 0;
}